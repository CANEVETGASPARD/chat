<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();

// On s'amuse à créer quelques variables de session dans $_SESSION
$_SESSION['password'] = $_POST['password'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>chat</title>
    <link rel="stylesheet" href="chat.css" />
</head>
<body>
<?php
    $mdp='kangourou';
    if (isset($_SESSION['password'])) 
    {
        if (htmlspecialchars($_SESSION['password']==$mdp)) {
            echo  "<div id='container'>
            <div id='fenetre_de_chat'>
                Chat : </br>
                <div id='conversation'></div>
            </div></br>
            <form id='form_chat'>
                <input type='text' id='pseudo' name='nom' value='pseudo'>
                <input type='text' id='message' name='message' value='message'>
                <input type='submit' id='envoyer' value='Envoyer'>
            </form></br>
            </div>";
        } else {
            echo  "<h2>vous avez le mauvais mot de passe</h2>";
        }
        
    }
    ?>
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="chat.js"></script>
</body>
</html>