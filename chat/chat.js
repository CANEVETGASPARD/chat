$(function(){

    maj_chat();

    $('#envoyer').click(function() {
        var nom = $('#pseudo').val();
        var message = $('#message').val();
        $.post('refresh_chat.php', {
            'nom': nom,
            'message': message
        }, maj_chat);
    });

    function maj_chat() {
        $("#conversation").load('ac.htm');
    }

    setInterval(maj_chat,2000);

});